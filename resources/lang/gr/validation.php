<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */


    'dimensions' => 'Το λογότυπο δεν έχει τις σωστές διαστάσεις λογοτύπου.',
    'email' => 'Η διέυθυνση ηλεκτρονικού ταχυδρομείου πρέπει να είναι έγγυρη διέυθυνση ηλεκτρονικού ταχυδρομείου',
    'image' => 'Το λογότυπο πρέπει να είναι εικόνα.',
    'not_in' => 'The selected :attribute is invalid.',
    'not_regex' => 'The :attribute format is invalid.',
    'numeric' => 'The :attribute must be a number.',
    'password' => 'Ο κωδικός πρόσβασης είναι λανθασμένος.',
    'required' => 'Το :attribute είναι υποχρεωτικό',
    'same' => 'Οι κωδικοί πρέπει να είναι οι ίδιοι.',
    'unique' => 'Υπάρχει ήδη κάποιος χρήστης με αυτή την διεύθυνση ηλεκτρονικού ταχυδρομείου.',


    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [],

];
