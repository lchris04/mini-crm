<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Αυτά τα στοιχεία δεν ταιριάζουν με τα στοιχεία μας',
    'throttle' => 'Πολλές προσπάθειες εισόδου στο σύστημα. Παρακαλώ προσπαθήστε ξανά σε :seconds δευτερόλεπτα.',

];
