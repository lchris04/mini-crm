<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'Ο κωδικός πρόσβασης σας έχει αλλάξει!',
    'sent' => 'Σας έχουμε στέλει μήνυμα στο ηλεκτρονικό σας ταχυδρομείο με τον κωδικό αλλαγής!',
    'throttled' => 'Παρακαλώ περημένετε πριν ξαναδοκιμάσετε.',
    'token' => 'Αυτό το τόκεν κωδικού δεν είναι έγκυρο.',
    'user' => "Δεν μπορούμε να βρούμε χρήστη με αυτό το ηλεκτρονικό ταχυδρομείο.",

];
