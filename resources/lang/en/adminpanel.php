<?php

return [
	'companies' => [
		'title' => 'Companies',
		'edit' => 'Edit Company',
		'add' => 'Add New Company',
		'messages' => [
			'add' => 'Company created successfully!',
			'edit' => 'Company updated successfully!',
			'delete' => 'Company deleted successfully!',
		],
		'fields' => [
			'name' => 'Name',
			'email' => 'Email',
			'phone' => 'Phone',
			'fax' => 'Fax',
			'address' => 'Address',
			'website' => 'Website',
		],
	],

	'employees' => [
		'title' => 'Employees',
		'edit' => 'Edit Employee',
		'add' => 'Add New Employee',
		'messages' => [
			'add' => 'Employee created successfully!',
			'edit' => 'Employee updated successfully!',
			'delete' => 'Employee deleted successfully!',
		],
		'fields' => [
			'name' => 'Name',
			'surname' => 'Surname',
			'email' => 'Email',
			'phone' => 'Phone',
			'company' => 'Company',
		],
	],

	'users' => [
		'title' => 'Users',
		'edit' => 'Edit User',
		'add' => 'Add New User',
		'messages' => [
			'add' => 'User created successfully!',
			'edit' => 'User updated successfully!',
			'delete' => 'User deleted successfully!',
		],
		'fields' => [
			'name' => 'Name',
			'surname' => 'Surname',
			'email' => 'Email',
			'perm-add' => 'Add permission',
			'perm-edit' => 'Edit permission',
			'perm-delete' => 'Delete permission',
		],
	],
	'login' => [
		'title' => 'Login',
		'email' => 'E-Mail Address',
		'password' => 'Password',
		'forgot-pass' => 'Forgot Your Password?',
	],
	'forgot-pass' => [
		'title' => 'Reset Password',
		'email' => 'E-Mail Address',
		'send-email' => 'Send Reset Password Link',
	],
	'reset-pass' => [
		'title' => 'Reset Password',
		'email' => 'E-Mail Address',
		'new-pass' => 'New password',
		'confirm-password' => 'Re-enter new password',
	],
	
	'no-employees-found' => 'No employees were found',
	'delete-existing-logo' => 'Delete existing logo',
	'select-new-logo' => 'Select New Logo',
	'select-logo' => 'Select Logo',
	'submit' => 'Submit',
	'add' => 'Αdd',
	'admin' => 'Admin',
	'dashboard' => 'Dashboard',
	'Change-Language' => 'Change Language',
	'logout' => 'Logout',
];