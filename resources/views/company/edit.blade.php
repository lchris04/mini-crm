@extends('layouts.master')

@section('title')
   @lang('adminpanel.companies.edit')
@endsection

@section('content')
    <div class='container'>
        <div class='row'>
            <div class='col-md-12'>
                <div class='card'>
                    <div class='card-header'>
                        <h3> @lang('adminpanel.companies.edit')</h3>
                    </div>
                    <div class='card-body'>
                        <form action="/companies/{{$company->id}}" method="POST" enctype="multipart/form-data">
                            {{csrf_field()}}
                            {{method_field('PUT')}}
                            <div class="form-group">
                                <label>@lang('adminpanel.companies.fields.name')</label>
                                <input type="text" name="name" class="form-control" value="{{$company->name}}" required>
                            </div>

                            <div class="form-group">
                                <label>@lang('adminpanel.companies.fields.email')</label>
                                <input type="text" name="email" value="{{$company->email}}"  class="form-control">
                            </div>

                            <div class="form-group">
                                <label>@lang('adminpanel.companies.fields.phone')</label>
                                <input type="text" name="phone" value="{{$company->phone}}"  class="form-control">
                            </div>

                            <div class="form-group">
                                <label>@lang('adminpanel.companies.fields.fax')</label>
                                <input type="text" name="fax" value="{{$company->fax}}" class="form-control">
                            </div>

                            <div class="form-group">
                                <label>@lang('adminpanel.companies.fields.address')</label>
                                <input type="text" name="address" value="{{$company->address}}"  class="form-control">
                            </div>

                            <div class="form-group">
                                <label>@lang('adminpanel.companies.fields.website')</label>
                                <input type="text" name="website" value="{{$company->website}}"  class="form-control">
                            </div>

                            <div class="form-check">
                                <center>
                                    <span class="btn btn-raised btn-round btn-default btn-file">
                                        <span class="fileinput-new">@lang('adminpanel.select-new-logo')</span>
                                        <input type="file" name="logo" />
                                    </span>

                                    {{-- only when it has a logo can remove it --}}
                                    @if($company->logo!=('noimage.jpg'))
                                        <label class="form-check-label">
                                        <input class="form-check-input" type="checkbox" name="deleteExisting">
                                            @lang('adminpanel.delete-existing-logo')
                                        <span class="form-check-sign">
                                            <span class="check"></span>
                                        </span>
                                    @endif    
                                </center>
                            </div>
                        
                            <div class="form-group">
                                <center>
                                    <button class="btn btn-primary">@lang('adminpanel.submit')</button>
                                </center> 
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

@endsection