@extends('layouts.master')

@section('title')
    @lang('adminpanel.companies.title')
@endsection

@section('content')
  <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            <h5 class="card-title">  @lang('adminpanel.companies.title')
              <div class="pull-right">
                {{-- show the add button only when the user has the permission to add stuffs --}}
                @if(Auth::user()->canAdd=="1") <a href="/companies/create" class='btn btn-success'><i class="fa fa-plus"></i> @lang('adminpanel.add')</a> @endif
              </div>
            </h5>
          </div>
          @if(count($companies)>0)
            <div class="card-body">
              <div class="table-responsive">
                <table id="datatable" class="table table-stripped" style="width:100%">
                    <thead class=" text-primary">
                    <th></th>
                    <th> @lang('adminpanel.companies.fields.name')</th>
                    <th> @lang('adminpanel.companies.fields.email')</th>
                    <th> @lang('adminpanel.companies.fields.phone')</th>
                    <th> @lang('adminpanel.companies.fields.fax')</th>
                    <th> @lang('adminpanel.companies.fields.address')</th>
                    <th> @lang('adminpanel.companies.fields.website')</th>
                    <th></th>
                    {{-- show these columns only when the user has the permissions --}}
                    @if(Auth::user()->canEdit=="1") <th></th> @endif
                    @if(Auth::user()->canDelete=="1")<th></th> @endif
                    </thead>
                    <tbody>
                      @foreach ($companies as $company)
                        <tr>
                          <td><img src={{asset("storage/companies/$company->logo")}} width=100%/></td>
                          <td>{{$company->name}}</td>
                          <td>{{$company->email}}</td>
                          <td>{{$company->phone}}</td>
                          <td>{{$company->fax}}</td>
                          <td>{{$company->address}}</td>
                          <td>{{$company->website}}</td>
                          <td>
                              <a href="/company-employees/{{$company->id}}" class="btn btn-default"><i class="fa fa-users"></i></a>
                          </td>
                          @if(Auth::user()->canEdit=="1")
                            <td>
                                <a href="/companies/{{$company->id}}/edit" class="btn btn-success"><i class="fa fa-edit"></i></a>
                            </td>
                          @endif
                          @if(Auth::user()->canDelete=="1")
                            <td>
                                <form action="/companies/{{$company->id}}" method="POST">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                    <button class="btn btn-danger"><i class="fa fa-trash"></i></button>
                                </form>
                            </td>
                          @endif
                        </tr>     
                      @endforeach
                    </tbody>  
                </table>
              </div>
            </div>
          @else
            <center><h4>No companies found</h4></center>
          @endif
        </div>
      </div>
    </div>     
@endsection

@section('scripts')
  <script>
    $(document).ready( function () {
      $('#datatable').DataTable();
    } );
  </script>
@endsection