@extends('layouts.master')

@section('title')
    @lang('adminpanel.companies.add')
@endsection

@section('content')
    <div class='container'>
        <div class='row'>
            <div class='col-md-12'>
                <div class='card'>
                    <div class='card-header'>
                        <h3> @lang('adminpanel.companies.add')</h3>
                    </div>
                    <div class='card-body'>
                        <form action="/companies" method="POST" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <div class="form-group">
                               <label>@lang('adminpanel.companies.fields.name')</label>
                                <input type="text" name="name" class="form-control" required>
                            </div>

                            <div class="form-group">
                               <label>@lang('adminpanel.companies.fields.email')</label>
                                <input type="text" name="email"  class="form-control">
                            </div>

                            <div class="form-group">
                                <label>@lang('adminpanel.companies.fields.phone')</label>
                                <input type="text" name="phone"  class="form-control">
                            </div>

                            <div class="form-group">
                                <label>@lang('adminpanel.companies.fields.fax')</label>
                                <input type="text" name="fax"  class="form-control">
                            </div>

                            <div class="form-group">
                                <label>@lang('adminpanel.companies.fields.address')</label>
                                <input type="text" name="address"  class="form-control">
                            </div>

                            <div class="form-group">
                               <label>@lang('adminpanel.companies.fields.website')</label>
                                <input type="text" name="website"  class="form-control">
                            </div>

                            <div class="form-check">
                                <center>
                                    <span class="btn btn-raised btn-round btn-default btn-file">
                                        <span class="fileinput-new">@lang('adminpanel.select-logo')</span>
                                        <input type="file" name="logo" />
                                    </span>
                                </center>
                            </div>
                        
                            <div class="form-group">
                                <center>
                                    <button class="btn btn-primary">@lang('adminpanel.submit')</button>
                                </center> 
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

@endsection