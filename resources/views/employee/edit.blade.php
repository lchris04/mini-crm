@extends('layouts.master')

@section('title')
   @lang('adminpanel.employees.edit')
@endsection

@section('content')
    <div class='container'>
        <div class='row'>
            <div class='col-md-12'>
                <div class='card'>
                    <div class='card-header'>
                        <h3>@lang('adminpanel.employees.edit')</h3>
                    </div>
                    <div class='card-body'>
                        <form action="/employees/{{$employee->id}}" method="POST">
                            {{csrf_field()}}
                             {{method_field('PUT')}}
                            <div class="form-group">
                                <label>@lang('adminpanel.employees.fields.name')</label>
                                <input type="text" name="name" class="form-control" value="{{$employee->name}}" required>
                            </div>

                            <div class="form-group">
                                <label>@lang('adminpanel.employees.fields.surname')</label>
                                <input type="text" name="surname"  class="form-control" value="{{$employee->surname}}" required>
                            </div>

                            <div class="form-group">
                                <label>@lang('adminpanel.employees.fields.email')</label>
                                <input type="text" name="email"  class="form-control" value="{{$employee->email}}">
                            </div>

                            <div class="form-group">
                                <label>@lang('adminpanel.employees.fields.phone')</label>
                                <input type="text" name="phone"  class="form-control" value="{{$employee->phone}}">
                            </div>

                            <div class="form-group">
                                <label>@lang('adminpanel.employees.fields.company')</label>
                                <select class="form-control" name="company" id="company">
                                @foreach ($companies as $company)
                                     <option value={{$company->id}}>{{$company->name}}</option>
                                @endforeach
                                </select>
                            </div>
                        
                            <div class="form-group">
                                <center>
                                    <button class="btn btn-primary">@lang('adminpanel.submit')</button>
                                </center> 
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

@endsection