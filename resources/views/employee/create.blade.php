@extends('layouts.master')

@section('title')
   @lang('adminpanel.employees.add')
@endsection

@section('content')
    <div class='container'>
        <div class='row'>
            <div class='col-md-12'>
                <div class='card'>
                    <div class='card-header'>
                        <h3> @lang('adminpanel.employees.add')</h3>
                    </div>
                    <div class='card-body'>
                        <form action="/employees" method="POST">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label>@lang('adminpanel.employees.fields.name')</label>
                                <input type="text" name="name" class="form-control" required>
                            </div>

                            <div class="form-group">
                                <label>@lang('adminpanel.employees.fields.surname')</label>
                                <input type="text" name="surname"  class="form-control">
                            </div>

                            <div class="form-group">
                                <label>@lang('adminpanel.employees.fields.email')</label>
                                <input type="text" name="email"  class="form-control">
                            </div>

                            <div class="form-group">
                                <label>@lang('adminpanel.employees.fields.phone')</label>
                                <input type="text" name="phone"  class="form-control">
                            </div>

                             <div class="form-group">
                                <label>@lang('adminpanel.employees.fields.company')</label>
                                <select class="form-control" name="company" id="company">
                                @foreach ($companies as $company)
                                     <option value={{$company->id}}>{{$company->name}}</option>
                                @endforeach
                                </select>
                            </div>
                        
                            <div class="form-group">
                                <center>
                                    <button class="btn btn-primary">@lang('adminpanel.submit')</button>
                                </center> 
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

@endsection