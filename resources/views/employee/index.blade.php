@extends('layouts.master')

@section('title')
  @lang('adminpanel.employees.title')
@endsection

@section('content')
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h5 class="card-title"> {{$title}}
            <div class="pull-right">
              {{-- show the add button only when the user has the permission to add stuffs --}}
              @if(Auth::user()->canAdd=="1") <a href="/employees/create" class='btn btn-success'><i class="fa fa-plus"></i> @lang('adminpanel.add')</a> @endif
            </div>
          </h5>
        </div>
          @if(count($employees)>0)
            <div class="card-body">
              <div class="table-responsive">
                <table class="table" id="datatable" style="width:100%">
                    <thead class=" text-primary">
                    <th> @lang('adminpanel.employees.fields.name')</th>
                    <th> @lang('adminpanel.employees.fields.surname')</th>
                    <th> @lang('adminpanel.employees.fields.email')</th>
                    <th> @lang('adminpanel.employees.fields.phone')</th>
                    <th> @lang('adminpanel.employees.fields.company')</th>
                    {{-- show these columns only when the user has the permissions --}}
                    @if(Auth::user()->canEdit=="1") <th></th> @endif
                    @if(Auth::user()->canDelete=="1")<th></th> @endif
                    </thead>
                    <tbody>
                      @foreach ($employees as $employeer)
                        <tr>
                          <td>{{$employeer->name}}</td>
                          <td>{{$employeer->surname}}</td>
                          <td>{{$employeer->email}}</td>
                          <td>{{$employeer->phone}}</td>
                          <td>{{$employeer->company->name}}</td>
                          @if(Auth::user()->canEdit=="1")
                            <td>
                                <a href="/employees/{{$employeer->id}}/edit" class="btn btn-success"><i class="fa fa-edit"></i></a>
                            </td>
                          @endif  
                          @if(Auth::user()->canDelete=="1")
                            <td>
                                <form action="/employees/{{$employeer->id}}" method="POST">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                    <button class="btn btn-danger"><i class="fa fa-trash"></i></button>
                                </form>
                            </td>
                          @endif
                        </tr>
                      @endforeach
                    </tbody> 
                </table>
              </div>
            </div>
          @else
            <center><h4>@lang('adminpanel.no-employees-found')</h4></center>
          @endif
        </div>
      </div>
    </div>     
@endsection

@section('scripts')
  <script>
    $(document).ready( function () {
      $('#datatable').DataTable();
    } );
  </script>
@endsection