@extends('layouts.master')

@section('title')
   Add New User
@endsection

@section('content')
    <div class='container'>
        <div class='row'>
            <div class='col-md-12'>
                <div class='card'>
                    <div class='card-header'>
                        <h3>Add New User</h3>
                    </div>
                    <div class='card-body'>
                        <form action="/users " method="POST">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label>@lang('adminpanel.users.fields.name')</label>
                                <input type="text" name="name" class="form-control">
                            </div>

                            <div class="form-group">
                                <label>@lang('adminpanel.users.fields.surname')</label>
                                <input type="text" name="surname"  class="form-control">
                            </div>

                            <div class="form-group">
                                <label>Email</label>
                                <input type="text" name="email"  class="form-control">
                                 @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="form-check">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="checkbox" name="canAdd">
                                    @lang('adminpanel.users.fields.perm-add')
                                    <span class="form-check-sign">
                                        <span class="check"></span>
                                    </span>
                                </label>
                            </div>

                            <div class="form-check">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="checkbox" name="canEdit">
                                    @lang('adminpanel.users.fields.perm-edit')
                                    <span class="form-check-sign">
                                        <span class="check"></span>
                                    </span>
                                </label>
                            </div>

                             <div class="form-check">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="checkbox" name="canDelete">
                                   @lang('adminpanel.users.fields.perm-delete')
                                    <span class="form-check-sign">
                                        <span class="check"></span>
                                    </span>
                                </label>
                            </div>

                            <div class="form-group">
                               <center><button class="btn btn-primary">@lang('adminpanel.submit')</button></center> 
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

@endsection