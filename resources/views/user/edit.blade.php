@extends('layouts.master')

@section('title')
   @lang('adminpanel.users.edit')
@endsection

@section('content')
    <div class='container'>
        <div class='row'>
            <div class='col-md-12'>
                <div class='card'>
                    <div class='card-header'>
                        <h3> @lang('adminpanel.users.edit')</h3>
                    </div>
                    <div class='card-body'>
                        <form action="/users/{{$user->id}}" method="POST">
                            {{csrf_field()}}
                            {{method_field('PUT')}}
                            <div class="form-group">
                                <label>@lang('adminpanel.users.fields.name')</label>
                                <input type="text" name="name" value="{{$user->name}}" class="form-control">
                            </div>

                            <div class="form-group">
                                <label>@lang('adminpanel.users.fields.surname')</label>
                                <input type="text" name="surname" value="{{$user->surname}}" class="form-control">
                            </div>

                            <div class="form-check">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="checkbox" name="canAdd" <?php echo ($user->canAdd==1 ? 'checked' : '');?>>
                                   @lang('adminpanel.users.fields.perm-add')
                                    <span class="form-check-sign">
                                        <span class="check"></span>
                                    </span>
                                </label>
                            </div>

                            <div class="form-check">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="checkbox" name="canEdit" <?php echo ($user->canEdit==1 ? 'checked' : '');?>>
                                   @lang('adminpanel.users.fields.perm-edit')
                                    <span class="form-check-sign">
                                        <span class="check"></span>
                                    </span>
                                </label>
                            </div>

                            <div class="form-check">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="checkbox" name="canDelete" <?php echo ($user->canDelete==1 ? 'checked' : '');?>>
                                    @lang('adminpanel.users.fields.perm-delete')
                                    <span class="form-check-sign">
                                        <span class="check"></span>
                                    </span>
                                </label>
                            </div>

                            <div class="form-group">
                               <center><button class="btn btn-primary">@lang('adminpanel.submit')</button></center> 
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

@endsection