@extends('layouts.master')

@section('title')
  @lang('adminpanel.users.title')
@endsection

@section('content')
     <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h5 class="card-title"> @lang('adminpanel.users.title')
                  <div class="pull-right">
                    <a href="/users/create" class='btn btn-success'><i class="fa fa-plus"></i>  @lang('adminpanel.add')</a>
                  </div>
                </h5>
              </div>
              @if(count($users)>0)
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table" id="datatable" style="width:100%">
                    <thead class=" text-primary">
                      <th> @lang('adminpanel.users.fields.name')</th>
                      <th> @lang('adminpanel.users.fields.surname')</th>
                      <th> @lang('adminpanel.users.fields.email')</th>
                      <th> @lang('adminpanel.users.fields.perm-add')</th>
                      <th> @lang('adminpanel.users.fields.perm-edit')</th>
                      <th> @lang('adminpanel.users.fields.perm-delete')</th>
                      <th></th>
                      <th></th>
                    </thead>
                    <tbody>
                      @foreach ($users as $user)
                        @if($user->userType!="admin") 
                            <tr>
                                <td>{{$user->name}}</td>
                                <td>{{$user->surname}}</td>
                                <td>{{$user->email}}</td>
                                 <td>@if($user->canAdd) Yes @else No @endif</td>
                                <td>@if($user->canEdit) Yes @else No @endif</td>
                                <td>@if($user->canDelete) Yes @else No @endif</td>
                                <td>
                                    <a href="/users/{{$user->id}}/edit" class="btn btn-success"><i class="fa fa-edit"></i></a>
                                </td>
                                <td>
                                    <form action="/users/{{$user->id}}" method="POST">
                                      {{ csrf_field() }}
                                      {{ method_field('DELETE') }}
                                      <button class="btn btn-danger"><i class="fa fa-trash"></i></button>
                                    </form>
                                </td>
                            </tr>
                           
                        @endif
                      @endforeach
                     </tbody>
                  </table>
                </div>
              </div>
              @else
                <center><h4>No users found</h4></center>
              @endif
            </div>
          </div>
        </div>     
@endsection

@section('scripts')
  <script>
    $(document).ready( function () {
      $('#datatable').DataTable();
    } );
  </script>
@endsection