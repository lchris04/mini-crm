<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('index');

Route::resource('companies', 'CompaniesController');

Route::resource('employees', 'EmployeesController');

Route::resource('users', 'UsersController');

//Custom route in order to display the company's employees
Route::get('/company-employees/{id}', 'EmployeesController@companyEmployees')->name('companyEmployees');

//Custom route in order to change the language
Route::get('lang/{lang}', ['as' => 'lang.switch', 'uses' => 'LanguageController@switchLang']);

Auth::routes();



