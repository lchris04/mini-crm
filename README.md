Steps:

	1) Clone the repo and cd into it

	2) Rename the .env.example to .env

	3) Create a new database with the name mini-CRM

	4) Run the command composer install 

	5) Run the command php artisan migrate

	6) Run the command php artisan db:seed --class=DatabaseSeeder

	7) Run the command php artisan storage:link

	8) Place the companies folder (attached in the email) in the public/storage directory

	9) Run the command php artisan serve

	10) Visit localhost:8000 in your browser

	11) Enjoy! 