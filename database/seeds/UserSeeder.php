<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => "admin",
            'surname' => "admin",
            'email' => "admin@admin.com",
            'userType' => "admin",
            'canAdd' => 1,
            'canDelete' => 1,
            'canEdit' => 1,
            'password' => Hash::make('password'),
        ]);

        DB::table('users')->insert([
            'name' => "user",
            'surname' => "user",
            'email' => "user@user.com",
            'userType' => "",
            'canAdd' => 1,
            'canDelete' => 0,
            'canEdit' => 1,
            'password' => Hash::make('password'),
        ]);

        DB::table('users')->insert([
            'name' => "user2",
            'surname' => "user2",
            'email' => "user2@user2.com",
            'userType' => "",
            'canAdd' => 1,
            'canDelete' => 0,
            'canEdit' => 0,
            'password' => Hash::make('password'),
        ]);
    }
}
