<?php

use Illuminate\Database\Seeder;

class EmployeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('employees')->insert([
            'name' => "Lenos",
            'surname' => "Christodoulou",
            'email' => "lenocch16@gmail.com",
            'companyID' => '1',
            'phone' => '96420050',
        ]);

        DB::table('employees')->insert([
            'name' => "Maria",
            'surname' => "Vrioni",
            'email' => "mvrioni@gmail.com",
            'companyID' => '2',
            'phone' => '96420030',
        ]);

        DB::table('employees')->insert([
            'name' => "Panayiota",
            'surname' => "Vrioni",
            'email' => "pvrioni@gmail.com",
            'companyID' => '3',
            'phone' => '96520030',
        ]);

        DB::table('employees')->insert([
            'name' => "Michalis",
            'surname' => "Christodoulou",
            'email' => "mchris@gmail.com",
            'companyID' => '4',
            'phone' => '96462539',
        ]);
    }
}
