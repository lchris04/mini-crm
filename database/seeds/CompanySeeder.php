<?php

use Illuminate\Database\Seeder;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('companies')->insert([
            'name' => "mmVirtual",
            'email' => "info@mmVirtual.com",
            'logo' => "70402325_2503617416364571_2381572078735196160_n_1591485246.jpg",
            'phone' => '25877888',
            'fax' => '25877888',
            'address' => '81 Omonia Avenue,Limassol',
            'website' => 'www.mmVirtual.com',
        ]);

        DB::table('companies')->insert([
            'name' => "pwc",
            'email' => "info@pwc.com",
            'logo' => "PwC_1591568122.png",
            'phone' => '22555000',
            'fax' => '22555001',
            'address' => '43 Demostheni Severi Avenue, Nicosia',
            'website' => 'www.pwc.com',
        ]);

        DB::table('companies')->insert([
            'name' => "Bank of Cyprus",
            'email' => "info@boc.com",
            'logo' => "boc-logo-small_1591484234.png",
            'phone' => '25155540',
            'fax' => '25155541',
            'address' => 'Pafou 1,Limassol',
            'website' => 'www.boc.com',
        ]);

        DB::table('companies')->insert([
            'name' => "Milkplan",
            'email' => "info@milkplan.com",
            'logo' => "26683_1591481097.jpg",
            'phone' => '24524403',
            'fax' => '24524404',
            'address' => 'Aradippou B Industrial Area, Aradippou',
            'website' => 'www.milkplan.com',
        ]);

    }
}
