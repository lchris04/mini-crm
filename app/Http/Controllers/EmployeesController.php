<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;
use App\Company;
use Lang;

class EmployeesController extends Controller
{

    public function __construct()
    {
        //With the following line we can avoid unauthorized users to reach this controller
        $this->middleware('auth');

        //With the following line we can avoid unauthorized users for adding stuffs to reach the create and store functions
        $this->middleware('canAdd',['except' => ['index' ,'update' ,'edit' ,'destroy', 'companyEmployees']]);

        //With the following line we can avoid unauthorized users for editing stuffs to reach the edit and update functions
        $this->middleware('canEdit',['except' => ['index' ,'create' ,'store' ,'destroy', 'companyEmployees']]);

        //With the following line we can avoid unauthorized users for deleting stuffs to reach the destroy function
        $this->middleware('canDelete',['except' => ['index' ,'create' ,'store' ,'update' ,'edit', 'companyEmployees']]);
    }

    public function index()
    {
        //Fetch all employees into the employees variable
        $employees = Employee::all();   
        
        //Pass the employees into the employee.index blade
        return view('employee.index')->with(['title'=> Lang::get('adminpanel.employees.title'),'employees'=>$employees]);   
    }

    public function show($id)
    {
        
    }

    public function create()
    {
        //Fetch all companies into the companies variable
        $companies = Company::all();

        //Pass all the companies into the employee.create blade
        return view('employee.create')->with('companies',$companies);
    }

    public function store(Request $request)
    {

        //Verify that the email is valid and unique
        $request->validate([
            'email' => 'email|unique:employees',
        ]);

        //Create Employee
        $employee = new Employee;
        $employee->name = $request->input('name');
        $employee->surname = $request->input('surname');
        $employee->email = $request->input('email');
        $employee->phone = $request->input('phone');
        $employee->companyID = $request->input('company');
        $employee->save();

        //Pass the employees into the companies.index blade
        return view('employee.index')->with([  'title'=>Lang::get('adminpanel.employees.title'),
                                                'success'=>Lang::get('adminpanel.employees.messages.add'),
                                                'employees'=>Employee::all()]);  
    }

    public function edit($id)
    {
        //Take the employee with that id and store him into the variable employee
        $employee = Employee::findOrFail($id);
        
        //Pass that employee into the employee.edit blade
        return view('employee.edit')->with(['employee'=>$employee,
                                            'companies'=>Company::all()]);
    }

    public function update(Request $request, $id)
    {
        //Take the employee with that id and store him into the variable employee
        $employee = Employee::findOrFail($id);

        //Verify that the email is valid
        $request->validate([
            'email' => 'email',
        ]);
        
        //Update Employee
        $employee->name = $request->get('name');
        $employee->surname = $request->get('surname');
        $employee->email = $request->get('email');
        $employee->phone = $request->get('phone');
        $employee->companyID = $request->get('company');
        $employee->save();

        //Redirect to the employees.index blade with the alert "Employee Updated Successfully"
        return redirect('/employees')->with('success',Lang::get('adminpanel.employees.messages.edit'));
    }

    public function destroy($id)
    {
        //Take the employee with that id and store him into the variable employee
        $employee = Employee::findOrFail($id);
        $employee->delete();

        //Redirect to the employeer.index blade with the alert "Employee Deleted Successfully"
        return redirect('/employees')->with('success',Lang::get('adminpanel.employees.messages.delete'));
    }

    public function companyEmployees($id){

        //Take all the employees of that company
        $employees = Employee::where('CompanyID','=',$id)->get();

        //Take company with that id 
        $company = Company::findOrFail($id);

        //Redirect to the employees.index with the proper title 
        return view('employee.index')->with(['title'=>$company->name.' - '.Lang::get('adminpanel.employees.title'),
                                            'employees'=>$employees]); 
    }

}
