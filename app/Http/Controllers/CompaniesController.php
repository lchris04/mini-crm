<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail;
use App\Company;
use Lang;
use Auth;

class CompaniesController extends Controller
{

    public function __construct()
    {
        //With the following line we can avoid unauthorized users to reach this controller
        $this->middleware('auth');

        //With the following line we can avoid unauthorized users for adding stuffs to reach the create and store functions
        $this->middleware('canAdd',['except' => ['index' ,'update' ,'edit' ,'destroy']]);

        //With the following line we can avoid unauthorized users for editing stuffs to reach the edit and update functions
        $this->middleware('canEdit',['except' => ['index' ,'create' ,'store' ,'destroy']]);

        //With the following line we can avoid unauthorized users for deleting stuffs to reach the destroy function
        $this->middleware('canDelete',['except' => ['index' ,'create' ,'store' ,'update' ,'edit']]);
    }

    public function index()
    {
        //Fetch all companies into the companies variable
        $companies = Company::all();   

        //Pass the companies into the company.index blade
        return view('company.index')->with('companies',$companies);   
    }

    public function show($id)
    {
        
    }

    public function create()
    {
        return view('company.create');
    }

    public function store(Request $request)
    {
        // Verify that the logo has minimum dimensions(100x100) and the email is valid and unique
        $request->validate([
            "name" => "required",
            "logo"  => "image|dimensions:min_width=100,min_height=100",
            'email' => 'email|unique:companies',
        ]);
        
         // Handle Logo Upload
         if($request->hasFile('logo'))
         {
            // Get filename with the extension
            $filenameWithExt = $request->file('logo')->getClientOriginalName();
            // Get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // Get just ext
            $extension = $request->file('logo')->getClientOriginalExtension();
            // Filename to store
            $fileNameToStore= $filename.'_'.time().'.'.$extension;
            // Upload Image
            $path = $request->file('logo')->storeAs('public/companies', $fileNameToStore);
        }
        else 
        {
            $fileNameToStore = 'noimage.jpg';
        }

        //Create Company
        $company = new Company;
        $company->name = $request->input('name');
        $company->email = $request->input('email');
        $company->logo = $fileNameToStore;
        $company->phone = $request->input('phone');
        $company->fax = $request->input('fax');
        $company->address = $request->input('address');
        $company->website = $request->input('website');
        $company->save();

        //Send an email to admin for the creation of a new company
        $data = array (
            'logo' => $fileNameToStore,
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'phone' => $request->input('phone'),
            'fax' => $request->input('fax'),
            'address' => $request->input('name'),
            'website' => $request->input('website'),
        );

        Mail::to('admin@admin.com')->send(new SendMail($data));

        //Pass the companies into the companies.index blade
        return view('company.index')->with(['success'=>Lang::get('adminpanel.companies.messages.add'),
                                                'companies'=>Company::all()]);  
    }

    public function edit($id)
    {
        //Take the company with that id and store it into the variable company
        $company = Company::findOrFail($id);
        
        //Pass that company into the company.edit blade
        return view('company.edit')->with('company',$company);
    }

    public function update(Request $request, $id)
    {
        $company = Company::findOrFail($id);
        
        // Verify that the logo has minimum dimensions (100x100) and the email is valid 
        $request->validate([
            "name" => "required",
            "logo"  => "dimensions:min_width=100,min_height=100",
            'email' => 'email',
        ]);

         // Handle Logo Upload
         if($request->hasFile('logo'))
         {
            // Get filename with the extension
            $filenameWithExt = $request->file('logo')->getClientOriginalName();
            // Get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // Get just ext
            $extension = $request->file('logo')->getClientOriginalExtension();
            // Filename to store
            $fileNameToStore= $filename.'_'.time().'.'.$extension;
            // Upload Image
            $path = $request->file('logo')->storeAs('public/companies', $fileNameToStore);
        }
        
        //If the checkbox (deleteExisting) equals to "on" (means that it is true) we have to delete the existing logo from the files
        if($request->input('deleteExisting') == "on")
        {
            Storage::delete('public/companies/'.$company->logo);
            $fileNameToStore = 'noimage.jpg';
        }

        $company->name = $request->get('name');
        $company->email = $request->get('email');

        //Only when the checkbox(deleteExisting) is true or a new logo has been uploaded we must make a change to the company logo field 
        if($request->hasFile('logo') || $request->input('deleteExisting') == "on")
        {
            $company->logo = $fileNameToStore;
        }
        
        $company->phone = $request->get('phone');
        $company->fax = $request->get('fax');
        $company->address = $request->get('address');
        $company->website = $request->get('website');

        $company->save();

        //Redirect to the users.index blade with the alert "User Updated Successfully"
        return redirect('/companies')->with('success',Lang::get('adminpanel.companies.messages.edit'));
    }

    public function destroy($id)
    {
        //Take the company with that id and store it into the variable company
        $company = Company::find($id);
        $company->delete();

        //If the company has already a logo, we have to delete the company's logo image
        if($company->logo != 'noimage.jpg'){
            Storage::delete('public/companies/'.$company->logo);
        }

        //Redirect to the company.index blade with the alert "Company Deleted Successfully"
        return redirect('/companies')->with('success',Lang::get('adminpanel.companies.messages.delete'));
    }

}
