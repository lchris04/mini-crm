<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;
use Validator;
use Lang;

class UsersController extends Controller
{

    public function __construct()
    {
        //With the following line we can avoid unautorized users to reach this controller
        $this->middleware('auth');

        //With the following line we can avoid users to reach this controller because only admin can reach it
        $this->middleware('admin');
    }

    public function index()
    {
        //Fetch all users into the users variable
        $users = User::all();   

        //Pass the users into the users.index blade
        return view('user.index')->with('users',$users);   
    }

    public function show($id)
    {

    }

    public function create()
    {
        return view('user.create');
    }

    public function store(Request $request)
    {
        //Verify that all the users have unique email
        $request->validate([
            'email' => 'required|email|unique:users',
        ]);

        //Create User
        $user = new User;
        $user->name = $request->get('name');
        $user->surname = $request->get('surname');
        $user->email = $request->get('email');
        $user->password = Hash::make("password");

        //If the request has the field canAdd, means that the checkbox is true
        if($request->has('canAdd'))
            $user->canAdd = 1;
        else
            $user->canAdd = 0;

        //If the request has the field canEdit, means that the checkbox is true
        if($request->has('canEdit'))
            $user->canEdit = 1;
        else
            $user->canEdit = 0;

       //If the request has the field canDelete, means that the checkbox is true
        if($request->has('canDelete'))    
            $user->canDelete = 1;
        else
            $user->canDelete = 0;    

        $user->save();

        //Redirect to the users.index blade with the alert "User Added Successfully"
        return redirect('/users')->with('success',lang::get('adminpanel.users.messages.add'));
    }

    public function edit($id)
    {
        //Take the user with that id and store him into the variable user
        $user = User::findOrFail($id);
        
        //Pass the user into the user.edit blade
        return view('user.edit')->with('user',$user);
    }

    public function update(Request $request, $id)
    {
        //Take the user with that id and store him into the variable user
        $user = User::findOrFail($id);
        
        $user->name = $request->get('name');
        $user->surname = $request->get('surname');

        //If the checkbox (canAdd) equals to "on" (means the checkbox was true) we have to make the field canAdd equals to 1
        if($request->get('canAdd')=="on")
            $user->canAdd = 1;
        else
            $user->canAdd = 0;

        //If the checkbox (canEdit) equals to "on" (means the checkbox was true) we have to make the field canEdit equals to 1
        if($request->get('canEdit')=="on")
            $user->canEdit = 1;
        else
            $user->canEdit = 0;

        //If the checkbox (canDelete) equals to "on" (means the checkbox was true) we have to make the field canDelete equals to 1
        if($request->get('canDelete')=="on")    
            $user->canDelete = 1;
        else
            $user->canDelete = 0;    

        $user->save();

        //Redirect to the users.index blade with the alert "User Updated Successfully"
        return redirect('/users')->with('success',lang::get('adminpanel.users.messages.edit'));
    }

    public function destroy($id)
    {
        //Take the user with that id and store him into the variable user
        $user = User::findOrFail($id);
        $user->delete();

        //Redirect to the users.index blade with the alert "User Deleted Successfully"
        return redirect('/users')->with('success',lang::get('adminpanel.users.messages.delete'));
    }

}
