<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class CanEdit
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //If that user is unauthorized to edit stuffs then redirect him to home 
        if(Auth::user()->canEdit=="1")
        {
            return $next($request);
        }
        else
        {
            return redirect('/');
        }
        
    }
}
