<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //If that user is not the admin then redirect him to home
        if(Auth::user()->userType=="admin")
        {
            return $next($request);
        }
        else
        {
            return redirect('/');
        }
    }
}
