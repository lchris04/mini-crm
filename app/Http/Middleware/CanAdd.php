<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class CanAdd
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //If that user is unauthorized to add stuffs then redirect him to home 
        if(Auth::user()->canAdd=="1")
        {
            return $next($request);
        }
        else
        {
            return redirect('/');
        }
        
    }
}
