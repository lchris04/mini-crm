<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class CanDelete
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //If that user is unauthorized to delete stuffs then redirect him to home 
        if(Auth::user()->canDelete=="1")
        {
            return $next($request);
        }
        else
        {
            return redirect('/');
        }
        
    }
}
