<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $table = 'employees';
    protected $primaryKey = 'id';

    protected $fillable = ['name','surname','email','phone'];

    //An employee belongs only to one company
    public function company()
    {
        return $this->belongsTo('App\Company', 'companyID');
    }
}
