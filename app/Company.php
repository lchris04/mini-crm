<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $table = 'companies';
    protected $primaryKey = 'id';

    protected $fillable = ['name','email','logo','phone','fax','address','website'];

    //A company has many employees
    public function employees()
    {
        return $this->hasMany('App\Employee','id');
    }
}
